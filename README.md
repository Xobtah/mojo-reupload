# mojo (reupload)

This is a reupload of the original Mojo project, which contained leaked credentials.

Mojo is a tool helping administrating fetish database. It is a command line tool that connects to MongoDB and toggles flags on users.

It should work on Windows only.

## Build

> MDB=MONGO_DB_URL cargo build

## Usage

> mojo.exe
