use std::{io::{stdout, Write}, time::Duration};
use mongodb::bson::{Bson, doc};
use mongodb::sync::Client;
use crossterm::{
    event::{poll, read, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEvent, KeyModifiers},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, Clear, ClearType},
    Result,
    QueueableCommand
};
use mongodb::options::FindOptions;
use mongodb::results::UpdateResult;

struct Usr {
    id: i64,
    first_name: String,
    last_name: String,
    username: String,
    bypass: bool,
    scam: bool
}

fn search(mongo: &Client, name: &str) -> Vec<Usr> {
    let regex_name = format!(".*{}.*", name.trim());
    let query = doc! {
        "$or": [
            { "first_name": { "$regex": regex_name.clone(), "$options" : "i" } },
            { "last_name": { "$regex": regex_name.clone(), "$options" : "i" } },
            { "username": { "$regex": regex_name.clone(), "$options" : "i" } }
        ]
    };

    let cursor = mongo.database("fetish").collection("users").find(query, Some(FindOptions::builder().limit(29).build())).unwrap();
    let mut result = Vec::new();

    for entry in cursor {
        match entry {
            Ok(doc) => {
                result.push(Usr {
                    id: doc.get("id").and_then(Bson::as_i64).or(Some(0)).unwrap(),
                    first_name: String::from(doc.get("first_name").and_then(Bson::as_str).or(Some("Unknown")).unwrap()),
                    last_name: String::from(doc.get("last_name").and_then(Bson::as_str).or(Some("Unknown")).unwrap()),
                    username: String::from(doc.get("username").and_then(Bson::as_str).or(Some("Unknown")).unwrap()),
                    bypass: doc.get("bypass").and_then(Bson::as_bool).or(Some(false)).unwrap(),
                    scam: doc.get("scam").and_then(Bson::as_bool).or(Some(false)).unwrap()
                });
            }
            Err(e) => eprintln!("Error : {:?}", e)
        }
    }

    result
}

fn toggle_scam(mongo: &Client, entries: &mut Vec<Usr>) -> mongodb::error::Result<UpdateResult> {
    let scam = entries.iter().any(|e| !e.scam);

    mongo.database("fetish").collection("users").update_many(
        doc! {
            "id": { "$in": entries.iter().map(|e| e.id).collect::<Vec<i64>>() }
        },
        doc! {
            "$set": { "scam": scam }
        },
        None
    )
}

fn toggle_bypass(mongo: &Client, entries: &mut Vec<Usr>) -> mongodb::error::Result<UpdateResult> {
    let bypass = entries.iter().any(|e| !e.bypass);

    mongo.database("fetish").collection("users").update_many(
        doc! {
            "id": { "$in": entries.iter().map(|e| e.id).collect::<Vec<i64>>() }
        },
        doc! {
            "$set": { "bypass": bypass }
        },
        None
    )
}

fn mojo() -> Result<()> {
    let mongo = Client::with_uri_str(env!("MDB")).unwrap();
    let mut result = Vec::new();
    let mut query = String::new();
    let mut stdout = stdout();

    execute!(stdout, Clear(ClearType::All)).unwrap();
    stdout.write(format!("> {}", query).as_bytes()).unwrap();
    stdout.flush().unwrap();

    loop {
        if poll(Duration::from_millis(1_000))? {
            let event = read()?;

            match event {
                Event::Key(KeyEvent {
                               code: KeyCode::Esc,
                               modifiers: _
                           }) => break,
                Event::Key(KeyEvent {
                               code: KeyCode::Char('s'),
                               modifiers: KeyModifiers::CONTROL
                           }) => { toggle_scam(&mongo, &mut result).unwrap(); },
                Event::Key(KeyEvent {
                               code: KeyCode::Char('b'),
                               modifiers: KeyModifiers::CONTROL
                           }) => { toggle_bypass(&mongo, &mut result).unwrap(); },
                Event::Key(KeyEvent {
                               code: KeyCode::Char(char),
                               modifiers: _
                           }) => { query.push(char); },
                Event::Key(KeyEvent {
                               code: KeyCode::Backspace,
                               modifiers: KeyModifiers::NONE
                           }) => if query.len() > 0 { query.remove(query.len() - 1); }, // TODO : Improve and test remove "é" last char
                _ => continue
            };

            result = search(&mongo, &query.trim());

            stdout.queue(Clear(ClearType::All)).unwrap();

            stdout.write(format!("\r{}", result.iter()
                .map(|usr| format!("[{}] {} {} (@{})",
                                   if usr.scam { "SCAM" } else if usr.bypass { "BYPASS" } else { "" },
                                   usr.first_name,
                                   usr.last_name,
                                   usr.username))
                .fold(String::new(), |a, b| a + &b + "\n")).as_bytes()).unwrap();

            stdout.write(format!("\r> {}", query).as_bytes()).unwrap();

            stdout.flush().unwrap();
        }
    }

    Ok(())
}

fn main() {
    enable_raw_mode().unwrap();

    let mut stdout = stdout();
    execute!(stdout, EnableMouseCapture).unwrap();

    if let Err(e) = mojo() {
        println!("Error: {:?}\r", e);
    }

    execute!(stdout, DisableMouseCapture).unwrap();

    disable_raw_mode().unwrap();
}
